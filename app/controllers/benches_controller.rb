class BenchesController < ApplicationController
    skip_before_action :verify_authenticity_token
    def index
        @benchmarks = Bench.all
    end

    def create
        @benchmark = Bench.new(bench_params)
        @game = Game.first_or_create(name: params[:game_name])
        @benchmark.game_id = @game.id

        respond_to do |format|
            if @benchmark.save
                @benchmark.parse_upload if @benchmark.upload.attached?
                format.html { redirect_to @benchmark, notice: 'Benchmark was successfully created.' }
                format.js
            else
                format.html { render :new }
                format.js
            end
        end
    end

    def update
        @benchmark = Bench.find(params[:id])
        if @benchmark.update(bench_params)
            respond_to do |format|
                if params[:attachment]
                    @benchmark.parse_upload
                    format.html { redirect_to bench_path(@benchmark) }
                else
                    format.html { redirect_to bench_path(@benchmark), flash: {success: 'Successfully updated benchmark'} }
                end
            end
        end
    end

    def show
        @benchmark = Bench.find(params[:id])
        @colors = Bench::TWENTY
    end

    private
    def bench_params
      params.require(:bench).permit(:game_id, :mesa_ver, :kernel, :renderer, :native, :upload)
    end
end
