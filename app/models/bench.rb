class Bench < ApplicationRecord
    #belongs_to :computer
    belongs_to :game
    has_many :inputs, dependent: :destroy
    has_many :metrics, dependent: :destroy
    
    has_one_attached :upload
    TWENTY    =  [ '#e6194b', '#3cb44b', '#4363d8', '#911eb4', '#f58231', '#E899DC', '#bcf60c', '#fabebe', '#008080', '#e6beff', '#9a6324', '#fffac8',
        '#800000', '#aaffc3', '#808000', '#000075', '#808080' ]

    def parse_upload
        require 'csv'
        download = upload.attachment.download
        parsed = CSV.parse(download)
        #return if parsed[0][0] != "v1"
        skip = skip_amount(self.game.name, parsed)
        log_data_pos = find_row(parsed, "FRAME METRICS")
        id = self.id
        parsed[log_data_pos].each_with_index do |col, x|
            data = []
            parsed.each_with_index do |row, i|
                if row[0].to_i > 0 && row[row.count - 1].to_i > skip
                    if col == "elapsed"
                        data.push(row[x].to_i - skip)
                    else
                        data.push(row[x])
                    end
                end
            end
            sum = 0
            data.each do |s|
                sum += s.to_i
            end
            Metric.create!(label: col, data: data, bench_id: id, avg: sum / data.count)
        end
        # parsed.each_with_index do |row, i|
        #     inputs.push(row) if i > 10 && row != parsed.last && row.last.to_i > skip
        # end
        # parsed = CSV.parse(download, headers: parsed[log_data_pos])
        # cpu = gpu = 0
        # inputs_count = inputs.count
        # self.update(raw: inputs, raw_json: inputs)
        # Input.import inputs_json
        # self.update_attribute(:kernel, parsed[system_info][4])
        # self.update_attribute(:renderer, parsed[system_info][9])
        # self.update_attribute(:native, parsed[system_info][7])
        # self.update_attribute(:gpu, parsed[1][2])
        # self.update_attribute(:driver, parsed[1][5])
        # self.update_attribute(:cpusched, parsed[1][6])
        # self.update_attribute(:sync, parsed[1][8])
        # self.upload.purge
    end

    def find_row(parsed, search)
        parsed.each_with_index do |parse, i|
            if (parse[0].include?(search))
                return i + 1
            end
        end
    end

    def skip_amount(game, parsed)
        if self.game.name == "Strange Brigade"
            return (1000000000 * 59) - parsed.last.last.to_i
        end
    end

    def fps 
        self.metrics.where(label: "fps").first.data
    end

    def frametime 
        self.metrics.where(label: "frametime").first.data
    end

    def elapsed 
        self.metrics.where(label: "elapsed").first.data
    end
end
