class PubChannel < ApplicationCable::Channel
  def subscribed
    stream_from "pub_channel"
  end
  
  def unsubscribed
  end
end