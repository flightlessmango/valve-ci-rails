class CreateMetrics < ActiveRecord::Migration[6.0]
  def change
    create_table :metrics do |t|
      t.integer :bench_id
      t.string  :label, limit: 15
      t.float   :data, array: true
      t.float   :avg
      t.timestamps
    end
  end
end
