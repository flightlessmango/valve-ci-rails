class CreateBenches < ActiveRecord::Migration[6.0]
  def change
    create_table :benches do |t|
      t.integer :game_id
      t.string  :mesa_ver
      t.string  :kernel
      t.string  :renderer
      t.string  :native
      t.float   :raw, array: true
      t.jsonb   :raw_json
      t.integer :avg_cpu
      t.integer :avg_gpu

      t.timestamps
    end
  end
end
