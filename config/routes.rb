Rails.application.routes.draw do
  resources :benches, :path => "benchmarks"
  root 'benches#index'
end
