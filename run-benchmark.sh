rm /tmp/*.csv
steam -applaunch 312670 -benchmark
echo "sleeping"
sleep 30
while pgrep -x "StrangeBrigade_" > /dev/null; do
    echo "Game still running"
    sleep 2
done
logfile=$(ls /tmp/*.csv)
upload='bench[upload]=@'
curl_post=$(curl --include --request POST http://78.73.83.124:8080/benchmarks -F 'game_name=Strange Brigade' -F 'bench[mesa_ver]=123')
benchmark_address=$(echo "$curl_post" | grep "Location" | head -1 | cut -d":" -f2- | tr -d ' ' | tr -d '\r')
curl --include --request PATCH $benchmark_address -F 'game_name=Strange Brigade' -F 'attachment=true' -F $upload$logfile -A 'mangohud' > /dev/null
rm /tmp/*.csv